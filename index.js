#!/usr/bin/node
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
// Load the math.js core
var core = require('mathjs/core');
require.config({
  paths: {
    d3: "//d3js.org/d3.v3.min"
  }
});

require(["d3"], function(d3) {
  console.log(d3.version);
});
// Create a new, empty math.js instance
// It will only contain methods `import` and `config`
var math = core.create();

// load the data types you need. Let's say you just want to use fractions,
// but no matrices, complex numbers, bignumbers, and other stuff.
//
// To load all data types:
//
math.import(require('./types/index'));

math.import(require('mathjs/lib/type/fraction'));
app.all('*',function(req,res,next) {
        res.header('Access-Control-Allow-Origin','*');
        next();
});
app.get('/', function(req, res){
  res.sendfile('index.html');
});
app.get('/croncheck', function(req, res) {
        res.send('OK');
});
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  socket.on('message',function (data) {
    console.log('message');
  });
  socket.on('subscribe', function (data,ret) {

  });
});

http.listen(3009, function(){
                 
}); 
