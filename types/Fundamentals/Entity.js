// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function Entity (scope, id, title, description) {
    this.scope= scope;
    this.id = id;
    this.title; = title;
    this.description = descriptioon;
    this.position = new GlobalPosition(id);
    this.valueConfidence = 
    this.valueFunctionList = new ValueFunctionList(id);
   
  }
  Entity.prototype.isMyType = true;
  Entity.prototype.toString = function () {
    return 'MyType:' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'Entity',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'number',
    to: 'Entity',
    convert: function (x) {
      // convert a number to MyType
      return new Entity(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return Entity;
}

exports.name = 'Entity';
exports.path = 'type';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
                                                                                              
