// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function VectorSpaceTransform (value) {
    this.value = value;
  }
  VectorSpaceTransform.prototype.isMyType = true;
  VectorSpaceTransform.prototype.toString = function () {
    return 'VectorSpaceTransform' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'VectorSpaceTransform',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'number',
    to: 'VectorSpaceTransform',
    convert: function (x) {
      // convert a number to MyType
      return new VectorSpaceTransform(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return VectorSpaceTransform;
}

exports.name = 'VectorSpaceTransform';
exports.path = 'type';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
E45: 'readonly' option is set (add ! to override)                                 
