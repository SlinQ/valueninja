// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function TranslationOffsetVector1D (value) {
    this.value = value;
  }
  TranslationOffsetVector1D.prototype.isMyType = true;
  TranslationOffsetVector1D.prototype.toString = function () {
    return 'TranslationOffsetVector1D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'TranslationOffsetVector1D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'number',
    to: 'TranslationOffsetVector1D',
    convert: function (x) {
      // convert a number to MyType
      return new TranslationOffsetVector1D(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return TranslationOffsetVector1D;
}

exports.name = 'TranslationOffsetVector1D';
exports.path = 'type';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
