// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function Vector1D (value) {
    this.value = new math.matrix([value]);

  }
  Vector1D.prototype.isMyType = true;
  Vector1D.prototype.toString = function () {
    return 'Vector1D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'Vector1D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Unbounded',
    to: 'Vector1D',
    convert: function (x) {
      // convert a number to MyType
      return new Vector1D(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return Vector1D;
}

exports.name = 'Vector1D';
exports.path = 'type/VectorTypes';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
