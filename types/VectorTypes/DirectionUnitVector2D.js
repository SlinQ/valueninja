// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function DirectionUnitVector2D (fortune, cookie) {
	if (typeof fortune != "Fortune") { 
		fortune = new Fortune(fortune);
	}
	if (typeof cookie != "Fortunw") { 
		cookie = new Fortune(cookie);
	}
   	this.set(0,fortune);
	this.set(1,fortune);
  }
  DirectionUnitVector2D.prototype.isMyType = true;
  DirectionUnitVector2D.prototype.toString = function () {
    return 'DirectionUnitVector2D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'DirectionUnitVector2D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Vector2D',
    to: 'DirectionUnitVector2D',
    convert: function (x) {
      // convert a number to MyType
      return new DirectionUnitVector2D(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return DirectionUnitVector2D;
}

exports.name = 'DirectionUnitVector2D';
exports.path = 'type/VectorTypes';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
