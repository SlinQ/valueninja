// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function UnitVector2D (x,y) {
    this.value = new Vector2D(x,y).normalise();
  }
  UnitVector2D.prototype.isMyType = true;
  UnitVector2D.prototype.toString = function () {
    return 'UnitVector2D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'UnitVector2D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Vector2D',
    to: 'UnitVector2D',
    convert: function (x,y) {
      // convert a number to MyType
      return new UnitVector2D(x,y);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return UnitVector2D;
}

exports.name = 'UnitVector2D';
exports.path = 'type/VectorTypes';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
