// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function Vector2D (x, y) {
    this.value = math.matrix([x, y]);
  }
  Vector2D.prototype.isMyType = true;
  Vector2D.prototype.toString = function () {
    return 'Vector2D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'Vector2D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });


  // return the construction function, this will
  // be added to math.type.MyType when imported
  return Vector2D;
}

exports.name = 'Vector2D';
exports.path = 'type/VectorTypes';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
