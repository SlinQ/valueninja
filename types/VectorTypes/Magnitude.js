// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function Magnitude (value) {
    this.value = new UnsignedUnbounded(Math.abs(value));
  }
  Magnitude.prototype.isMyType = true;
  Magnitude.prototype.toString = function () {
    return 'Magnitude' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'Magnitude',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Vector2D',
    to: 'Magnitude',
    convert: function (x) {
      // convert a number to MyType
      return new Magnitude(x.magnitude());
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return Magnitude;
}

exports.name = 'Magnitude';
exports.path = 'type/VectorTypes';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
