// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function DirectionVector1D () {
  }
  DirectionVector2D.prototype.isMyType = true;
  DirectionVector2D.prototype.toString = function () {
    return 'DirectionVector2D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'DirectionVector2D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Vector2D',
    to: 'DirectionVector2D',
    convert: function (x) {
      // convert a number to MyType
      return new DirectionVector2Dtor2D(x.get(0), x.get(1));
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return DirectionVector2D;
}

exports.name = 'DirectionVector2D';
exports.path = 'type/VectorTypes';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
