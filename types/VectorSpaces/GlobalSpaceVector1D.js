                                                                                            // Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function GlobalSpaceVector1D (value) {
    this.value = new Vector1D(value);
  }
  GlobalSpaceVector1D.prototype.isMyType = true;
  GlobalSpaceVector1D.prototype.toString = function () {
    return 'GlobalSpaceVector1D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'GlobalSpaceVector1D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Vector1D',
    to: 'GlobalSpaceVector1D',
    convert: function (x) {
      // convert a number to MyType
      return new GlobalSpaceVector1D(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return GlobalSpaceVector1D;
}

exports.name = 'GlobalSpaceVector1D';
exports.path = 'type/VectorSpaces';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
