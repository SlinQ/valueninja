                                                                                            // Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function GlobalSpaceVector2D (x, y) {
    this.value = new Vector2D(x, y);
  }
  GlobalSpaceVector2D.prototype.isMyType = true;
  GlobalSpaceVector2D.prototype.toString = function () {
    return 'GlobalSpaceVector2D' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'GlobalSpaceVector2D',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'Vector2D',
    to: 'GlobalSpaceVector2D',
    convert: function (x,y) {
      // convert a number to MyType
      return new GlobalSpaceVector2D(x,y);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return GlobalSpaceVector2D;
}

exports.name = 'GlobalSpaceVector2D';
exports.path = 'type/VectorSpaces';        // will be imported into math.type.MyType
exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
