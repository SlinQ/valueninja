// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function UnsignedDiscreteBounded (value, min, max) {
    this.value = new Send(value, true, min, max, true);
  }
  UnsignedDiscreteBounded.prototype.isMyType = true;
  UnsignedDiscreteBounded.prototype.toString = function () {
    return 'UnsignedDiscreteBounded' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'UnsignedDiscreteBounded',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'number',
    to: 'UnsignedDiscreteBounded',
    convert: function (x) {
      // convert a number to MyType
      return new UnsignedDiscreteBounded(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return UnsignedDiscreteBounded;
}

exports.path = 'type/Primatives';        // will be imported into math.type.MyType

exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
                                                                                              
