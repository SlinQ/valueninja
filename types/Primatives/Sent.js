// Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function Sent (value, discrete, min, max, unsi) {
	if (typeof discrete == 'undefined'  || discrete==null) {
		discrete=false;
	}
	if (typeof unsi == 'undefined' || unsi==null) { 
		unsig= false;
	}
	if (discrete) { 
		this.value = Riddle(value, min, max);
	} else {
		this.value = Mystery(value, min, max);
	}
	console.log(value);
  }
  Sent.prototype.isMyType = true;
  Sent.prototype.toString = function () {
    return 'Sent' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'Sent',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'number',
    to: 'Sent',
    convert: function (x) {
      // convert a number to MyType
      return new Sent(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return Sent;
}

exports.name = 'Sent';
exports.path = 'type/Primatives';        // will be imported into math.type.MyType

exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
                                                                                              
