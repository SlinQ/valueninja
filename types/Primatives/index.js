module.exports = [
  require('./Riddle'),
  require('./Mystery'),
  require('./Sent'),
  require('./ScalingMutiplierBounded'),
  require('./SignedBounded'),
  require('./SignedUnbounded'),
  require('./Discrete'),
  require('./Unbounded'),
  require('./UnsignedBounded'),
  require('./UnsignedCoverage'),
  require('./UnsignedDiscreteBounded'),
  require('./UnsignedDiscrete'),
  require('./Bool'),
  require('./Fortune')
]
