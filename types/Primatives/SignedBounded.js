                                                                                            // Note: This file is used by the file ./index.js


// factory function which defines a new data type MyType
function factory(type, config, load, typed) {

  // create a new data type
  function SignedBounded (value, max) {
    this.value = new Sent(value, false, null, max);;
  }
  SignedBounded.prototype.isMyType = true;
  SignedBounded.prototype.toString = function () {
    return 'SignedBounded' + this.value;
  };

  // define a new data type
  typed.addType({
    name: 'SignedBounded',
    test: function (x) {
      // test whether x is of type MyType
      return x && x.isMyType;
    }
  });

  // define conversions if applicable
  typed.addConversion({
    from: 'number',
    to: 'SignedBounded',
    convert: function (x) {
      // convert a number to MyType
      return new SignedBounded(x);
    }
  });

  // return the construction function, this will
  // be added to math.type.MyType when imported
  return SignedBounded;
}

exports.name = 'SignedBounded';
exports.path = 'type/Primatives';        // will be imported into math.type.MyType

exports.factory = factory;
exports.lazy = false;         // disable lazy loading as this factory has side
                              // effects: it adds a type and a conversion.
